import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
const axios = require('axios').default;

export default function useEffectCase3Screen() {
  const [refresh, setRefresh] = useState(false);
  const [temperature, setTemperature] = useState({});

  useEffect(() => {
    console.log('useEffect starts executing');

    const source = axios.CancelToken.source();

    axios
      .get(
        'https://api.openweathermap.org/data/2.5/weather',
        {
          params: {q: 'delhi', appid: '81480bc07d37f3692c8c55208c0ee156'},
        },
        {
          cancelToken: source.token,
        },
      )
      .then((response) => {
        console.log('Response data', response);
        const temp = response.data.main;
        setTemperature({
          feelsLike: (temp.feels_like - 273.15).toFixed(1),
          humidity: temp.humidity,
          pressure: temp.pressure,
          currentTemp: (temp.temp - 273.15).toFixed(1),
          maximumTemp: (temp.temp_max - 273.15).toFixed(1),
          minimumTemp: (temp.temp_min - 273.15).toFixed(1),
        });
        setRefresh(false);
      })
      .catch(function (err) {
        //check if request was canceled
        if (axios.isCancel(err)) {
          console.log('Request canceled', err.message);
        } else {
          // handle error
          console.log(err);
        }
      });

    // cancel the request (the message parameter is optional)
    return function cleanup() {
      source.cancel('Operation canceled by the user.');
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [refresh]);

  console.log('temp outside:', temperature);

  return (
    <>
      <View style={styles.container}>
        <Text style={styles.textStyle}>Delhi</Text>
        <Text style={styles.textStyle}>
          Current Temperature: {temperature.currentTemp}
        </Text>
        <Text style={styles.textStyle}>
          Maximum Temperature: {temperature.maximumTemp}
        </Text>
        <Text style={styles.textStyle}>
          Minimum Temperature: {temperature.minimumTemp}
        </Text>
        <Text style={styles.textStyle}>Humidity: {temperature.humidity}</Text>
        <Button title="Refresh" onPress={() => setRefresh(true)} />
        <View style={styles.ainvayi} />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: 20,
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  textStyle: {
    fontSize: 20,
  },
  ainvayi: {height: 200},
});
