/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Dimensions,
  FlatList,
  TouchableOpacity,
  Alert,
} from 'react-native';
import CustomButton from '../common/CustomButton';
export default function useEffectCase2Screen() {
  const [task, setTask] = useState('');
  const [errorTask, setErrorTask] = useState('');
  const [disable, setDisable] = useState(false);
  const [removeItem, setRemoveItem] = useState('');
  const [taskList, setTaskList] = useState(['Learn React']);
  const renderItem = (item) => {
    return (
      <View style={styles.taskContainerStyle}>
        <TouchableOpacity onPress={() => setRemoveItem(item)}>
          <Text style={styles.taskTextStyle}>{item}</Text>
        </TouchableOpacity>
      </View>
    );
  };
  useEffect(() => {
    setTaskList(taskList.filter((todoItem) => removeItem !== todoItem));
  }, [removeItem]);
  useEffect(() => {
    taskList.length === 7 ? setDisable(true) : setDisable(false);
  }, [taskList]);
  const addTask = () => {
    if (task && !taskList.includes(task)) {
      setTaskList([...taskList, task]);
      setErrorTask('');
    } else if (!task) {
      setErrorTask('Enter Task');
    } else if (taskList.includes(task)) {
      setErrorTask('Task already Present');
    }
  };
  return (
    <View style={styles.containerStyle}>
      <Text style={styles.textStyle}>ToDo List ({taskList.length})</Text>
      <TextInput
        onChangeText={(taskText) => setTask(taskText)}
        autoCorrect={false}
        autoCapitalize="none"
        placeholder="What is to be done"
        style={styles.textInput}
      />
      {errorTask ? (
        <Text style={styles.errorTextStyle}>{errorTask}</Text>
      ) : null}
      <CustomButton
        children="ADD TASK"
        pressed={disable ? () => Alert.alert('Maximum Limit Reached') : addTask}
      />
      <FlatList
        data={taskList}
        keyExtractor={(item) => item}
        renderItem={({item}) => renderItem(item)}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  errorTextStyle: {
    fontWeight: '600',
    textAlign: 'center',
    color: 'red',
  },
  textStyle: {
    fontWeight: 'bold',
    fontSize: 30,
    textAlign: 'center',
    marginVertical: 30,
  },
  containerStyle: {marginHorizontal: 50},
  taskContainerStyle: {
    marginVertical: 12,
    marginHorizontal: 30,
    paddingVertical: 12,
    borderRadius: 10,
    backgroundColor: '#007aff',
  },
  taskTextStyle: {
    color: 'white',
    fontSize: 20,
    fontFamily: 'arial',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  textInput: {
    alignSelf: 'center',
    fontFamily: 'arial',
    height: Dimensions.get('screen').height / 20,
    marginVertical: 10,
    width: Dimensions.get('screen').width * 0.8,
    marginHorizontal: 40,
    borderRadius: 10,
    borderWidth: 1.5,
    borderColor: 'grey',
    fontSize: 14,
    paddingHorizontal: 16,
    color: 'black',
  },
});
