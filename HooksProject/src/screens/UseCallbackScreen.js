import React, {useCallback, useEffect, useRef, useState, memo} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';

const TotalSum = ({calculateSum}) => {
  const myFuncRef = useRef(calculateSum);

  console.log('Callback', myFuncRef.current === calculateSum);

  const [sum, setSum] = useState(0);
  useEffect(() => {
    setSum(calculateSum());
  }, [calculateSum]);

  return <Text>Total sum = {sum}</Text>;
};

const MyTotalSum = memo(TotalSum);

export default function UseCallbackScreen() {
  const [name, setName] = useState('');
  const [num1, setNum1] = useState(0);
  const [num2, setNum2] = useState(0);
  const [num3, setNum3] = useState(0);
  const [num4, setNum4] = useState(0);

  const calculateSumCallback = useCallback(() => {
    console.log('I was called 1');
    // for (let i = 0; i < 100000000; i++) {}
    return Number(num1) + Number(num2);
  }, [num1, num2]);

  const calculateSumCallback2 = useCallback(() => {
    console.log('I was called 2');
    return Number(num3) + Number(num4);
  }, [num3, num4]);

  return (
    <View style={styles.container}>
      <Text>Enter Name :</Text>
      <TextInput
        style={styles.inputStyle}
        autoCapitalize="none"
        autoCorrect={false}
        value={name}
        onChangeText={(newText) => setName(newText)}
      />

      <Text>Enter Number 1 :</Text>

      <TextInput
        style={styles.inputStyle}
        autoCapitalize="none"
        autoCorrect={false}
        onChangeText={(newText) => setNum1(newText)}
      />
      <Text>Enter Number 2 :</Text>

      <TextInput
        style={styles.inputStyle}
        autoCapitalize="none"
        autoCorrect={false}
        onChangeText={(newText) => setNum2(newText)}
      />
      <Text>Enter Number 3 :</Text>

      <TextInput
        style={styles.inputStyle}
        autoCapitalize="none"
        autoCorrect={false}
        onChangeText={(newText) => setNum3(newText)}
      />
      <Text>Enter Number 4 :</Text>

      <TextInput
        style={styles.inputStyle}
        autoCapitalize="none"
        autoCorrect={false}
        onChangeText={(newText) => setNum4(newText)}
      />
      {/*<Text>1 sum :{calculateSumCallback()}</Text> */}
      <Text>2 sum : {calculateSumCallback2()}</Text>

      <MyTotalSum calculateSum={calculateSumCallback} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: 30,
  },
  inputStyle: {
    margin: 10,
    height: 30,
    borderWidth: 1,
    borderColor: 'black',
  },
});
