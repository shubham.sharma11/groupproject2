   # Baazi Premier League Mobile Application
[![Build Status](https://travis-ci.org/klugjo/hexo-autolinker.svg?branch=master)](https://travis-ci.org/klugjo/hexo-autolinker)
# OVERVIEW
    An online gaming application that offers  games -  Rummy,Fruit Chop, 8 ball 3D Pool , Knifecut and many more
  
   # Main Technologies Used
        React Native
            A framework for building native apps with React.

        Mobx state tree
            Mobx is a state container for JavaScript apps.
    
# Requirements
   * IOS SDK (comes with Xcode https://developer.apple.com/xcode/)
   * Android SDK (Comes with Andorid SDK or as independent binary https://developer.android.com/studio/releases/sdk-tools?gclid=EAIaIQobChMIqMr288f95QIVVQwrCh2aQge8EAAYASAAEgLQ9fD_BwE)
   * Java 1.8
   * Node version > 10.0.0 (& NPM)
   * Yarn (https://yarnpkg.com/lang/en/docs/install/#mac-stable)
   * Yalc (https://www.npmjs.com/package/yalc)
   
   # Installation
    For iOS: react-native run-ios
    For Android: react-native run-android
        Note: The Android emulator needs to be running prior to executing this command.
    
    
## Steps for setting up iOS application
    1: Clone
    2: npm install 
    3: ios-> cd ios -> pod install -> cd .. -> npm run ios
    4: Create file bplApp-Bridging-Header.h in ios folder
        
       ** //
        //  Use this file to import your target's public headers that you would like to expose to Swift.
        //**

    5: Create file test.swift in ios folder
        
       ** //
        //  test.swift
        //  bplApp
        //
        import Foundation**

    
    6: Add library libswiftWebKit.tbd in xcode workspace file ‘bplApp.xcworkspace’
        (steps-> bplApp Project in sidebar->TARGETS->bplApp->general-> libraries)

        
    ** In case of this error ** 
        bpl_project/bpl_client/bplApp/ios/Pods/AppCenterReactNativeShared/AppCenterReactNativeShared/AppCenterReactNativeShared.framework/AppCenterReactNativeShared' for architecture arm64”
    Solution: Xcode workspace->click on ’bplApp’ in sidebar -> Build Settings->Architectures ->Excluded Architectures ->
    Debug-> Any iOS simulator SDK : arm64
    Release-> Any iOS simulator SDK : arm64


    ** In case of this error ** 
    /Xcode/DerivedData/bplApp-dkniaooycmcnugafhnywalazgpqu/Build/Products/Debug-iphonesimulator/FBSDKCoreKit/FBSDKCoreKit.modulemap' not found
         1: rm -rf ios/Pods
         2: rm ios/Podfile.lock 
         3: cd ios  
         4: pod cache clean --all  
         5: pod repo update
         6: pod install         
         7: Xcode workspace->click on ‘Pods’ in sidebar ->Build Settings-> Architectures-> 
         8: Excluded Architectures ->
            Debug-> Any iOS simulator SDK : arm64
            Release-> Any iOS simulator SDK : arm64

    
     

   
   ## Third party libraries :
    Firebase
    CleverTap : provides the functionality to integrate app analytics and marketing
    App center
    Branch io
    Facebook Developer Account
    Apple Account
   
   
  ## Update Android APK feature:
    1: Upload the new APK on S3 in baazimobilegaming-apks and copy the Object URL (the name of APK should be in ${ENV}_${Date}_app-release.apk 
    2: Change the version name, version code and apkUrl in androidApkConfig.json ( i.e. on S3 in baazimobilegaming-apks)
    3: Give both file Public access
    4: Go on Admin panel, and under app config change the version of force update same as the version code in androidApkConfig.json


   
   
   
   
   
